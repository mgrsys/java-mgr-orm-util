package com.magorasystems.hibernate.pg.orm.util.array;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.*;
import java.util.Collection;

/**
 * @author tokar;
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class CollectionUserType<T extends Collection> implements UserType {

    private final Class<T> clazz;

    private final String sqlTypeForClass;

    public CollectionUserType(Class<T> clazz, String sqlTypeForClass) {
        this.clazz = clazz;
        this.sqlTypeForClass = sqlTypeForClass;
    }


    /**
     * Constante contenant le type SQL "Array".
     */
    protected static final int[] SQL_TYPES = {Types.ARRAY};

    /**
     * Return the SQL type codes for the columns mapped by this type. The
     * codes are defined on <tt>java.sql.Types</tt>.
     *
     * @return int[] the typecodes
     * @see java.sql.Types
     */
    public final int[] sqlTypes() {
        return SQL_TYPES;
    }

    /**
     * The class returned by <tt>nullSafeGet()</tt>.
     *
     * @return Class
     */
    public final Class returnedClass() {
        return clazz;
    }

    /**
     * Retrieve an instance of the mapped class from a JDBC resultset. Implementors
     * should handle possibility of null values.
     *
     * @param resultSet a JDBC result set.
     * @param names     the column names.
     * @param session   SQL en cours.
     * @param owner     the containing entity
     * @return Object
     * @throws org.hibernate.HibernateException exception levée par Hibernate
     *                                          lors de la récupération des données.
     * @throws java.sql.SQLException            exception SQL
     *                                          levées lors de la récupération des données.
     */
    @Override
    public final Object nullSafeGet(final ResultSet resultSet, final String[] names, final SharedSessionContractImplementor session, final Object owner) throws HibernateException, SQLException {

        T collection;
        try {
            collection = clazz.newInstance();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }

        Array arrayByName = resultSet.getArray(names[0]);
        if (arrayByName == null) {
            return collection;
        }

        for (Object obj : (Object[]) arrayByName.getArray()) {
            collection.add(obj);
        }

        return collection;
    }

    /**
     * Write an instance of the mapped class to a prepared statement. Implementors
     * should handle possibility of null values. A multi-column type should be written
     * to parameters starting from <tt>index</tt>.
     *
     * @param statement a JDBC prepared statement.
     * @param value     the object to write
     * @param index     statement parameter index
     * @param session   sql en cours
     * @throws org.hibernate.HibernateException exception levée par Hibernate
     *                                          lors de la récupération des données.
     * @throws java.sql.SQLException            exception SQL
     *                                          levées lors de la récupération des données.
     */
    @Override
    public final void nullSafeSet(final PreparedStatement statement, final Object value, final int index, final SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            statement.setNull(index, SQL_TYPES[0]);
        } else {

            T collection = (T) value;
            Array array = session.connection().createArrayOf(sqlTypeForClass, collection.toArray());
            statement.setArray(index, array);
        }
    }

    @Override
    public final Object deepCopy(final Object value) throws HibernateException {
        return value;
    }

    @Override
    public final boolean isMutable() {
        return false;
    }

    @Override
    public final Object assemble(final Serializable arg0, final Object arg1) throws HibernateException {
        return null;
    }

    @Override
    public final Serializable disassemble(final Object arg0) throws HibernateException {
        return null;
    }

    @Override
    public final boolean equals(final Object x, final Object y) throws HibernateException {
        if (x == y) {
            return true;
        } else if (x == null || y == null) {
            return false;
        } else {
            return x.equals(y);
        }
    }

    @Override
    public final int hashCode(final Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public final Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }
}