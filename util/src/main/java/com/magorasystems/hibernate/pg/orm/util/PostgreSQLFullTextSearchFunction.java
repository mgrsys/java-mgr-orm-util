package com.magorasystems.hibernate.pg.orm.util;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com). 2016.
 *
 * @author Denis Kataev
 */
public class PostgreSQLFullTextSearchFunction implements SQLFunction {
    @Override
    public boolean hasArguments() {
        return true;
    }

    @Override
    public boolean hasParenthesesIfNoArguments() {
        return false;
    }

    @Override
    public Type getReturnType(Type firstArgumentType, Mapping mapping) throws QueryException {
        return new BooleanType();
    }

    @Override
    public String render(Type firstArgumentType, List arguments, SessionFactoryImplementor factory) throws QueryException {
        if (arguments.size() != 2) {
            throw new IllegalArgumentException("function required 2 arguments");
        }
        String field = (String) arguments.get(0);
        String value = (String) arguments.get(1);

        return String.format("(%s @@ to_tsquery('simple', %s))", field, value);
    }
}
