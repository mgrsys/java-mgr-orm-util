package com.magorasystems.hibernate.pg.orm.util;

import java.util.Arrays;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Developed by Magora Team (magora-systems.com). 2016.
 *
 * @author Denis Kataev
 */
public class TextSearchHelper {


    public String buildQuery(String plainText) {
        return Optional.ofNullable(plainText)
                .map(String::trim)
                .map(this::split)
                .filter(items -> items.length > 0)
                .map(this::join)
                .filter(this::isNotEmpty)
                .orElse("");
    }

    private String[] split(String value) {
        return value.toLowerCase()
                .trim()
                .replaceAll("[\\(\\)\\p{Cntrl}!&|']", " ")
                .replaceAll(":", "\\\\:")
                .replaceAll("\\s+", " ")
                .split(" ");
    }

    private String join(String[] tokens) {
        StringJoiner joiner = new StringJoiner(" & ");
        Arrays.stream(tokens)
                .filter(this::isNotEmpty)
                .map(x -> x + ":*")
                .forEach(joiner::add);
        return joiner.toString();
    }


    private boolean isNotEmpty(String val) {
        return !(val == null || val.isEmpty());
    }
}
