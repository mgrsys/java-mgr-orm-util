package com.magorasystems.hibernate.pg.orm.util.array;

import java.util.HashSet;

public class HashSetOfIntegerUserType extends CollectionUserType<HashSet> {
    public HashSetOfIntegerUserType() {
        super(HashSet.class, "integer");
    }
}
