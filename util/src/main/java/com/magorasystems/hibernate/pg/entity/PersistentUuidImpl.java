package com.magorasystems.hibernate.pg.entity;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Setter
@RequiredArgsConstructor
@ToString(exclude = {})
@MappedSuperclass
public abstract class PersistentUuidImpl implements Serializable {

    public static final String UUID_TYPE_NAME = "pg-uuid";
    public static final String GENERIC_GENERATOR_NAME = "system-uuid";

    @Id
    @Column(name = "ID", nullable = false, updatable = false, unique = true, length = 36)
    @Type(type = UUID_TYPE_NAME)
    @GeneratedValue(generator = GENERIC_GENERATOR_NAME)
    @Access(AccessType.PROPERTY)
    private UUID id;

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersistentUuidImpl that = (PersistentUuidImpl) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
