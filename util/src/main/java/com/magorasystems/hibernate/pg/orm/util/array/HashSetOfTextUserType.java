package com.magorasystems.hibernate.pg.orm.util.array;

import java.util.HashSet;

public class HashSetOfTextUserType extends CollectionUserType<HashSet> {
    public HashSetOfTextUserType() {
        super(HashSet.class, "text");
    }
}
