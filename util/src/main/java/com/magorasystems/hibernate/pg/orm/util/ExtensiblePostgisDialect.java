package com.magorasystems.hibernate.pg.orm.util;

import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.spatial.dialect.postgis.PostgisDialect;

import java.util.Map;

/**
 * @author tokar;
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class ExtensiblePostgisDialect extends PostgisDialect {

    public ExtensiblePostgisDialect() {
    }

    public ExtensiblePostgisDialect(Map<String, SQLFunction> sqlFunctions) {
        sqlFunctions.forEach(this::registerFunction);
    }
}
