package com.magorasystems.hibernate.pg.entity;

public final class EntityConstants {

    public static final String NOT_DELETED_CLAUSE = "is_deleted=false";
    public static final String ACTIVE_CLAUSE = "is_deleted=false and is_blocked=false";

    public final static String SEQUENCE_GENERATOR_NAME = "optimized-sequence";

    private EntityConstants() {
    }
}
