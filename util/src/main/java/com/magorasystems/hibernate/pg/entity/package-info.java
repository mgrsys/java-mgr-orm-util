
@GenericGenerators({
        @GenericGenerator(
                name = PersistentUuidImpl.GENERIC_GENERATOR_NAME,
                strategy = "uuid2"
        ),
        @GenericGenerator(
                name = EntityConstants.SEQUENCE_GENERATOR_NAME,
                strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
                parameters = {
                        @Parameter(name = SequenceStyleGenerator.OPT_PARAM, value = "pooled-lo"),
                        @Parameter(name = SequenceStyleGenerator.CONFIG_PREFER_SEQUENCE_PER_ENTITY, value = "true"),
                        @Parameter(name = SequenceStyleGenerator.CONFIG_SEQUENCE_PER_ENTITY_SUFFIX, value = "_ID_SEQ")
                }
        )
})


@TypeDefs({
        @TypeDef(name = "integerSet", typeClass = HashSetOfIntegerUserType.class, parameters = {}),
        @TypeDef(name = "textSet", typeClass = HashSetOfTextUserType.class, parameters = {}),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class, parameters = {})
})
package com.magorasystems.hibernate.pg.entity;

import com.magorasystems.hibernate.pg.orm.util.array.HashSetOfIntegerUserType;
import com.magorasystems.hibernate.pg.orm.util.array.HashSetOfTextUserType;
import com.magorasystems.hibernate.pg.orm.util.json.JsonBinaryType;
import org.hibernate.annotations.*;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
