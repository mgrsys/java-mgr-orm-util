package com.magorasystems.hibernate.pg.orm.util;

import org.hibernate.dialect.PostgreSQL82Dialect;

/**
 * Created by kemenov on 20.10.2016.
 */
public class FTSCustomDialect extends PostgreSQL82Dialect {

    public static final String FTS = "fts";

    public FTSCustomDialect() {
        registerFunction("fts", new PostgreSQLFullTextSearchFunction());
    }
}